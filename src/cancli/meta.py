#!./runmodule.sh

'''
A command line interface to send and receive CAN bus messages.
'''

__version__ = '1.3.1'
